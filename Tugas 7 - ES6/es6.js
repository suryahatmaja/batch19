//soal 1 - Mengubah fungsi menjadi fungsi arrow
console.log("--------------------------------------------------");
console.log("soal 1 - Mengubah fungsi menjadi fungsi arrow");
console.log("--------------------------------------------------");
console.log("\n======= ES5 =======");
const golden = function goldenFunction () {
    console.log("this is golden!!")
}
golden();

console.log("\n======= ES6 =======");
const goldenArrow = () => {
    console.log("this is golden!!")
}
goldenArrow();

//soal 2 - Sederhanakan menjadi Object literal di ES6
console.log("\n--------------------------------------------------");
console.log("Soal 2 - Sederhanakan menjadi Object literal di ES6");
console.log("--------------------------------------------------");
console.log("\n======= ES5 =======");
const newFunction = function literal (firstName, lastName){
    return {
        firstName : firstName,
        lastName : lastName,
        fullName : function() {
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("William", "Imoh").fullName();

console.log("\n======= ES6 =======");
const newFunctionEs6 = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName() {
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunctionEs6("William", "Imoh").fullName();
console.log(newFunctionEs6("William", "Imoh").firstName);

//soal 3 - Destructuring
console.log("\n--------------------------------------------------");
console.log("soal 3 - Destructuring");
console.log("--------------------------------------------------");

console.log("\n======= ES5 =======");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;

console.log(firstName, lastName, destination, occupation);

console.log("\n======= ES6 =======");
const newObject1 = {
    firstName1: "Harry",
    lastName1: "Potter Holt",
    destination1: "Hogwarts React Conf",
    occupation1: "Deve-wizard Avocado",
    spell1: "Vimulus Renderus!!!"
}
const { firstName1, lastName1, destination1, occupation1, spell1 } = newObject1;

console.log(firstName1, lastName1, destination1, occupation1);

//soal 4 - Array Spreading
console.log("\n--------------------------------------------------");
console.log("soal 4 - Array Spreading");
console.log("--------------------------------------------------");
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

console.log("\n======= ES5 =======");
const combined = west.concat(east);
console.log(combined);

console.log("\n======= ES6 =======");
const combined1 = [...west, ...east];
console.log(combined1);

//soal 5 - Template Literals
console.log("\n--------------------------------------------------");
console.log("soal 5 - Template Literals");
console.log("--------------------------------------------------");
console.log("\n======= ES5 =======");
const planet = 'earth';
const view = 'glass';
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam';
console.log(before);

console.log("\n======= ES6 =======");
var after = `Lorem ${view} dolor sit amet, 
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`;
console.log(after);