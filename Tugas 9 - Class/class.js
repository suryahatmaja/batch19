//1. Animal Class
console.log("------------------------------------");
console.log("1. Animal Class");
console.log("------------------------------------");

console.log("===== Release 0 =====");
class Animal {
    constructor(animalName) {
        this.name = animalName
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\n===== Release 1 =====");
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name, cold_blooded) {
      super(name, cold_blooded);
      this.legs = 2;
    }
    yell() {
      return console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name, legs) {
      super(name, legs);
      this.cold_blooded = true;
    }
    jump() {
      return console.log("hop hop");
    }
}

console.log("*Animal 1:")
var sungokong = new Ape("kera sakti")
console.log("Name: " + sungokong.name);
console.log("Legs: " + sungokong.legs);
console.log("Cold Blooeded: " + sungokong.cold_blooded);
sungokong.yell() // "Auooo"

console.log("\n*Animal 2:")
var kodok = new Frog("buduk")
console.log("Name: " + kodok.name);
console.log("Legs: " + kodok.legs);
console.log("Cold Blooeded: " + kodok.cold_blooded);
kodok.jump() // "hop hop" 

//2. Function to Class
console.log("\n------------------------------------");
console.log("2. Function to Class");
console.log("------------------------------------");

console.log("==== Using function ====");
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
}
  
var clock = new Clock({template: 'h:m:s'});
clock.start(); 
clock.stop();


console.log("\n==== Using Class ====")
class Clock2 {
    // Code di sini
    constructor({template}) 
    {
        this.template = template;
    }
    render()
    {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }
    stop()
    {
        clearInterval(this.timer);
    };
    
    start()
    {
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    };
}

var clock2 = new Clock2({template: 'h:m:s'});
clock2.start(); 
//clock2.stop(); 
