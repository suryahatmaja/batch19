//Soal No. 2 (Promise Baca Buku)
console.log("------------------------------------")
console.log("Soal No. 2 (Promise Baca Buku)");
console.log("------------------------------------")

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let availableTime = 10000;
let bookId = 0;

function letsReadBooks()
{
    readBooksPromise (availableTime, books[bookId])
        .then(function (timeCheck)
        {
            availableTime = timeCheck;
            bookId++;
            if (bookId < books.length)
            {
                letsReadBooks();
            }
        })
        .catch(function(error){})
}

letsReadBooks();