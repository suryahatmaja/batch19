
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import VideoItem from './components/videoItem';
// import data from './data.json';

export default class App extends Component {
    constructor() {
        super();
        this.state = {
          value: '',
        };
      }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
            <Image source={require('./images/logo.png')} style={{ width: 375, height: 102 }} />  
        </View>

        <Text style={styles.pageTitle}>Login</Text>
        <View style = {styles.input}>
            <Text>Username / Email</Text>
            <View>
                <TextInput
                style={styles.textBox}
                onChangeText={(text) => this.setState({ value: text })}
                value={this.state.value}
                />
                <Text>{this.state.value}</Text>
            </View>
            
            <Text style={{marginTop: 16}}>Password</Text>
            <View>
                <TextInput
                style={styles.textBox}
                onChangeText={(text) => this.setState({ value: text })}
                value={this.state.value}
                />
                <Text>{this.state.value}</Text>
            </View>
        </View>

        <TouchableOpacity style={styles.buttonLogin}>
            <Text style={{color: '#FFFFFF', fontSize: 24,}}>Masuk</Text>            
        </TouchableOpacity>

        <Text style={{color: '#3EC6FF', fontSize: 24, marginTop: 16, marginBottom: 16,}}>atau</Text>

        <TouchableOpacity style={styles.buttonRegister}>
            <Text style={{color: '#FFFFFF', fontSize: 24,}}>Daftar ?</Text>            
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems:'center',
      //flexDirection: 'column',
      
    },
    logo: {
        height: 110,
        width: 380,
        marginTop: 63,
        alignItems:'center',
        //borderWidth: 2,
        //borderColor: '#003366',
    },
    pageTitle: {
        color: '#003366',
        fontSize: 24,
        marginTop: 70,

    },
    textBox: { 
        width: 294,
        height: 48,
        borderColor: '#003366', 
        borderWidth: 1,
    },
    buttonLogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#3EC6FF",
        width: 140,
        height: 40,
        borderRadius: 16,
        marginTop: 32,
    },
    buttonRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#003366",
        width: 140,
        height: 40,
        borderRadius: 16,
    },
    input: {
        color: '#003366',
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 40,
    },
  });