//soal 1
console.log("soal 1:")
var nama = "Jenita"
var peran = "Guard"

if (nama == "") {
// Output untuk Input nama = '' dan peran = ''
console.log("Nama harus diisi!");
}
else if (peran == "") {
//Output untuk Input nama = 'John' dan peran = ''
console.log("Halo ".concat(nama, ", Pilih peranmu untuk memulai game!"));
}
else if (nama == 'Jane' && peran == 'Penyihir') {
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
console.log("Selamat datang di Dunia Werewolf, " + nama);
console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
} 
else if (nama == 'Jenita' && peran == 'Guard') {
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
console.log("Selamat datang di Dunia Werewolf, " + nama);
console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
//"Selamat datang di Dunia Werewolf, Jenita"
//"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
}
else if (nama == 'Junaedi' && peran == 'Werewolf') {
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
console.log("Selamat datang di Dunia Werewolf, " + nama);
console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");

//"Selamat datang di Dunia Werewolf, Junaedi"
//"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 
}

//soal 2
console.log("\nsoal 2:")

var hari = 29; 
var bulan = 22; 
var tahun = 2000;
var bulan1 = ""
var hari1 = 31


switch(bulan) {
    case 1:   { bulan1 = 'Januari'; hari1 = 31; break; }
    case 2:   { bulan1 = 'Februari'; hari1 = 28;  break; }
    case 3:   { bulan1 = 'Maret'; hari1 = 31;  break; }
    case 4:   { bulan1 = 'April'; hari1 = 30;  break; }
    case 5:   { bulan1 = 'Mei'; hari1 = 31;  break; }
    case 6:   { bulan1 = 'Juni'; hari1 = 30;  break; }
    case 7:   { bulan1 = 'Juli'; hari1 = 31;  break; }
    case 8:   { bulan1 = 'Agustus'; hari1 = 31;  break; }
    case 9:   { bulan1 = 'September'; hari1 = 30;  break; }
    case 10:   { bulan1 = 'Oktober'; hari1 = 31;  break; }
    case 11:   { bulan1 = 'November'; hari1 = 30;  break; }
    case 12:   { bulan1 = 'Desember'; hari1 = 31;  break; }
    default:  { bulan1; hari1;}
}

if(tahun >= 1900 && tahun <= 2200) {
    if (tahun%4 == 0 && bulan == 2){
        hari1 = 29;
    }
    if (bulan1 == "") {
        console.log("input bulan harus antara 1 - 12");
    }
    else if (hari < 1 || hari > hari1) {
        console.log("input hari harus antara 1 - " + hari1);
    }
    else {
        console.log(hari+' '+bulan1+' '+tahun);
    }
}
else {
    console.log("input tahun harus antara 1900 - 2200");
}