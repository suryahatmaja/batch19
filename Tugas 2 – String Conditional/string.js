//soal 1
console.log("\nsoal 1:")
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log(word.concat(" ", second, " ", third, " ", fourth, " ", fifth, " ", sixth, " ", seventh))

//soal 2
console.log("\nsoal 2:")
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.substr(5, 5); // lakukan sendiri 
var fourthWord= sentence.substr(sentence.indexOf(thirdWord) + thirdWord.length + 1, 2); // lakukan sendiri 
var fifthWord = sentence.substr(sentence.indexOf(fourthWord) + fourthWord.length + 1, 2); // lakukan sendiri 
var sixthWord = sentence.substr(sentence.indexOf(fifthWord) + fifthWord.length + 1, 5); // lakukan sendiri 
var seventhWord = sentence.substr(sentence.indexOf(sixthWord) + sixthWord.length + 1, 6); // lakukan sendiri 
var eighthWord = sentence.substr(sentence.indexOf(seventhWord) + seventhWord.length + 1, 9); // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)

//soal 3
console.log("\nsoal 3:")
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substr(sentence2.indexOf(exampleFirstWord2) + exampleFirstWord2.length + 1, 10); // do your own! 
var thirdWord2 = sentence2.substr(sentence2.indexOf(secondWord2) + secondWord2.length + 1, 2); // do your own! 
var fourthWord2 = sentence2.substr(sentence2.indexOf(thirdWord2) + thirdWord2.length + 1, 2); // do your own! 
var fifthWord2 = sentence2.substr(sentence2.indexOf(fourthWord2) + fourthWord2.length + 1, 4); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//soal 4
console.log("\nsoal 4:")
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4,14); // do your own! 
var thirdWord3 = sentence3.substr(sentence3.indexOf(secondWord3) + secondWord3.length + 1, 2); // do your own! 
var fourthWord3 = sentence3.substr(sentence3.indexOf(thirdWord3) + thirdWord3.length + 1, 2); // do your own! 
var fifthWord3 = sentence3.substr(sentence3.indexOf(fourthWord3) + fourthWord3.length + 1, 4); // do your own! 

var firstWordLength = exampleFirstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 