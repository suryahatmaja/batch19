import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas13/LoginScreen';
import AboutScreen from './Tugas13/AboutScreen';
import Skill from './Tugas14/App';
import Tugas15 from './Tugas15/index';
import TugasNavigation from './TugasNavigation/index';

//export default function App() {
const App = () => {
  return (
    //<YoutubeUI/>
    // <LoginScreen />
    // <AboutScreen /> //Belum selesai
    // <Skill />
    // <Index />
    // <Tugas15/>
    <TugasNavigation />
   
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#df0e0f',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;