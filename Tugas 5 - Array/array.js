//Soal No. 1 (Range)
console.log("---------------------------------");
console.log("Soal No. 1 (Range)");
console.log("---------------------------------");
function range(startNum, finishNum) 
{
    if (startNum === undefined || finishNum === undefined)
    {
        return -1;
    }
    var arr = [];
    var step = 1;
    if (startNum < finishNum)
    {
        for (var i = startNum; i <= finishNum; i += step)
        {
            arr.push(i);
        }
    }
    else
    {
        for (var i = startNum; i >= finishNum; i -= step)
        {
            arr.push(i);
        }
    }
    return arr;
} 

console.log(range(1, 10));//[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal No. 2 (Range wth step)
console.log("\n---------------------------------");
console.log("Soal No. 2 (Range wth step)")
console.log("---------------------------------");
function rangeWithStep(startNum, finishNum, step = 1) 
{
    if (startNum === undefined || finishNum === undefined)
    {
        return -1;
    }
    var arr = [];
    if (startNum < finishNum)
    {
        for (var i = startNum; i <= finishNum; i += step)
        {
            arr.push(i);
        }
    }
    else
    {
        for (var i = startNum; i >= finishNum; i -= step)
        {
            arr.push(i);
        }
    }
    return arr;
} 

console.log(rangeWithStep(1, 10, 2));// [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3 (Sum of Range)
console.log("\n---------------------------------");
console.log("Soal No. 3 (Sum of Range)");
console.log("---------------------------------");
function sum(startNum, finishNum, step = 1) 
{
    if (finishNum === undefined)
    {
        if (startNum === undefined)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    var rng = rangeWithStep (startNum, finishNum, step);
    var jumlah = 0;
    for (var i = 0; i < rng.length; i++)
    {
        jumlah += rng[i];
    }
    return jumlah;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal No. 4 (Array multidimensi)
console.log("\n---------------------------------");
console.log("Soal No. 4 (Array multidimensi");
console.log("---------------------------------");
function dataHandling(input)
{
    for(var i = 0; i < input.length; i++)
    {
        console.log("Nomor ID: " + input[i][0]);
        console.log("Nama lengkap: " + input[i][1]);
        console.log("TTL: " + input[i][2] + " " + input[i][3]);
        console.log("Hobi: " + input[i][4]);
        console.log("\n");
    }
//  Nomor ID:  0001
//  Nama Lengkap:  Roman Alamsyah
//  TTL:  Bandar Lampung 21/05/1989
//  Hobi:  Membaca
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input);

//Soal No. 5 (Balik kata)
console.log("\n---------------------------------");
console.log("Soal No. 5 (Balik kata)")
console.log("---------------------------------");
function balikKata(input)
{
    var kata = "";
    for (var i = input.length - 1; i >= 0; i--)
    {
        kata += input[i];
    }
    return kata;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal No. 6 (Metode Array)
console.log("\n---------------------------------");
console.log("Soal No. 6 (Metode Array)")
console.log("---------------------------------");
function month(input)
{
    switch(input) {
        case "01":   { return 'Januari'}
        case "02":   { return 'Februari'}
        case "03":   { return 'Maret'}
        case "04":   { return  'April'}
        case "05":   { return  'Mei'}
        case "06":   { return 'Juni'}
        case "07":   { return 'Juli'}
        case "08":   { return 'Agustus'}
        case "09":   { return 'September'}
        case "10":   { return 'Oktober'}
        case "11":   { return 'November'}
        case "12":   { return 'Desember'}
        default:  { return "bulan tidak terdefinisi"}
    }
}
function dataHandling2(input)
{
    //["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
    //["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
        var nama_modifikasi = "Roman Alamsyah Elsharawy";
        var tempat_lahir_detail = "Provinsi " + input[2];
        var tanggal_lahir = input[3];
        var jenis_kelamin = "Pria";
        var sekolah = "SMA Internasional Metro";
        input.splice(1,4,nama_modifikasi, tempat_lahir_detail, tanggal_lahir, jenis_kelamin, sekolah);
                
        console.log(input);
        
        var date = input[3].split("/");
        console.log(month(date[1]));
        
        var date_sorted = date.slice(0).sort(function(value1, value2) {return value2 - value1});
        console.log(date_sorted);
        console.log(date.join("-"));

        console.log(input[1].slice(0,14));
        // console.log("Nomor ID: " + input[0]);
        // console.log("Nama lengkap: " + input[1]);
        // console.log("TTL: " + input[2] + " " + input[3]);
        // console.log("Jenis Kelamin: " + input[4]);
        // console.log("Sekolah: " + input[5]);
        // console.log("\n");
}

var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input2);

