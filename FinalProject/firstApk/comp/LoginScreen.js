
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
// import Checkbox from '@material-ui/core/Checkbox';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AboutScreen from './AboutScreen';

function LoginScreen ({ navigation }) {
  // const [checked, setChecked] = React.useState(true);

  // const handleChange = (event) => {
  //   setChecked(event.target.checked);
  // };
    return (
//      <View style={styles.container}>
      <KeyboardAvoidingView
        behavior = {Platform.OS == 'ios' ? 'padding' : 'height'}
        style = {styles.container}
      >
        <ScrollView>
           <View style = {styles.containerView}>
            <View style={styles.logo}>
                <Image source={require('../img/logo.png')} style={{ width: 375, height: 102 }} />  
            </View>

            <Text style={styles.pageTitle}>LOGIN</Text>
            <View style = {styles.input}>
                <Text>Username / Email</Text>
                <View>
                    <TextInput
                    style={styles.textBox}
                    onChangeText={(text) => this.setState({ value: text })}
                    // value={this.state.value}
                    />
                    {/* <Text>{this.state.value}</Text> */}
                </View>
                
                <Text style={{marginTop: 16}}>Password</Text>
                <View>
                    <TextInput
                    style={styles.textBox} secureTextEntry = {true}
                    />
                </View>
            </View>
            <View style={{flexDirection: 'row', width: 294,}}>
              <View style={{width: 147,}}>
              {/* <Checkbox
        checked={checked}
        onChange={handleChange}
        inputProps={{ 'aria-label': 'primary checkbox' }}
      /> */}
                <Text>Ingat Saya</Text>
              </View>
              <View style={{width: 147, alignItems: 'flex-end'}}>
                <Text>Lupa Kata Kunci?</Text>
              </View>
          </View>

            <TouchableOpacity style={styles.buttonLogin}
            onPress={() => navigation.navigate('Dashboard', { screen: 'Home' })}>
                <Text style={{color: '#FFFFFF', fontSize: 20,}}>Masuk</Text>            
            </TouchableOpacity>
            <Text style={{color: '#3EC6FF', fontSize: 16, }}>|</Text>
            <Text style={{color: '#3EC6FF', fontSize: 16, }}>atau</Text>
            <Text style={{color: '#3EC6FF', fontSize: 16, }}>|</Text>

            <TouchableOpacity style={styles.buttonRegister}>
                <Text style={{color: '#FFFFFF', fontSize: 20,}}>Daftar ?</Text>            
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      //<View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,      
    },
    containerView: {
      alignItems:'center',
    },
    logo: {
        height: 110,
        width: 380,
        marginTop: 63,
    },
    pageTitle: {
        color: '#003366',
        fontSize: 24,
        marginTop: 50,
        textAlign:'center',
        fontWeight: 'bold',

    },
    textBox: { 
        width: 294,
        height: 36,
        borderColor: '#003366', 
        borderWidth: 1,
        padding: 10,
    },
    buttonLogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#3EC6FF",
        width: 140,
        height: 40,
        borderRadius: 16,
        marginTop: 32,
    },
    buttonRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#003366",
        width: 140,
        height: 40,
        borderRadius: 16,
        marginTop: 8,
    },
    input: {
        color: '#003366',
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 40,       
    },
  });

  export default LoginScreen;