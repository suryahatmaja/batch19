import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View, TouchableWithoutFeedback, ScrollView} from "react-native";


// function numberWithCommas(x) {
//   var parts = x.toString().split(".");
//   return parts[0].replace(/\B(?=(\d{3})+(?=$))/g, ",") + (parts[1] ? "." + parts[1] : "");
// }

export default class WorldScreen extends Component {
  state = {
    data: []
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    // const response = await fetch("https://randomuser.me/api?results=500");
    const response = await fetch("https://api.kawalcorona.com/");
    const json = await response.json();
    this.setState({ data: json });
  };
  
  
  renderItem = ({ item }) => {
    return (
      <View style={styles.row}>
        
      {/* <TouchableWithoutFeedback  onPress ={() => showItemDetails(item.attributes.Country_Region)}> */}
      {/* <View style={styles.listItem}> */}
          <Text style={styles.countryName}>{item.attributes.Country_Region}</Text>
          <Text style={{}}>Confirmed: {item.attributes.Confirmed} | 
          Death: {item.attributes.Deaths} |
          Recovered: {item.attributes.Recovered} |
          Active: {item.attributes.Active}</Text>
      {/* </View> */}
      {/* </TouchableWithoutFeedback> */}
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>________ World Case ________</Text>
        </View>
        <ScrollView > 
          <FlatList 
            data={this.state.data}
            // numColumns={2}
            keyExtractor={(x, i) => i}
            renderItem={this.renderItem}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    flex: 1,
    //justifyContent: "center",
    // alignItems: "center",
   // backgroundColor: "#F5FCFF"
  },
  headerContainer: {
    alignItems: 'center',
  },
  headerText: {
    paddingBottom: 30,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
    fontSize: 24,
  },
  listItem: {
    // maxWidth: Dimensions.get('window').width /2,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 4,
    fontSize: 10,
  },
  row: {
    padding: 15,
    marginBottom: 5,
    backgroundColor: 'skyblue',
    // flexDirection: 'column',
        // justifyContent: 'center',
        alignItems: 'stretch',
    borderRadius: 15,
  },
  countryName: {
    fontSize: 20,
  },
});
