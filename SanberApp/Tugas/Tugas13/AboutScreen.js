
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import VideoItem from './components/videoItem';
// import data from './data.json';

export default class App extends Component {
    constructor() {
        super();
        this.state = {
          value: '',
        };
      }
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.pageTitle}>Tentang Saya</Text>
          {/* <FontAwesome5 name='user-circle'/> */}
          <View style={styles.logo}>
            <Icon name= "person" size={180} />  
            {/* <Image source={require('./images/profile_blank.jpg')} style={{ width: 200, height: 200 }} />   */}
          </View>

          <View style = {styles.Profile}>  
            <Text style={styles.ProfileName}>Mukhlis Hanafi</Text>
            <Text style={styles.ProfileJob}>React Native Developer</Text>
          </View>

          <View style={styles.box1}>
            <Text style={{color: '#003366', padding: 10, textAlign: 'left'}}>Portofolio</Text>
            <View style={styles.box2}>
                <View style={styles.box3}>
                  <FontAwesome5  style={styles.icon} name='gitlab' size={50}  color={'#3EC6FF'}/> 
                  <Text>@mukhlis</Text>
                </View>
                <View style={styles.box3}>
                  <FontAwesome5  style={styles.icon} name='github' size={50} color={'#3EC6FF'}/>
                  <Text>@mukhlis-h</Text>
                </View>
            </View>
          </View>

          <View style={styles.box1}>
            <Text style={{color: '#003366', padding: 10, }}>Hubungi Saya</Text>
            <View style={styles.box2_}>
            <View style={styles.box3_}>
                  <FontAwesome5 style={styles.icon} name='facebook' size={50}  color={'#3EC6FF'}/>
                  <Text>mukhlis.hanafi</Text>
                </View>
                <View style={styles.box3_}>
                <FontAwesome5  style={styles.icon} name='instagram' size={50}  color={'#3EC6FF'}/>
                  <Text>@mukhlis-hanafi</Text>
                </View>
                <View style={styles.box3_}>
                <FontAwesome5  style={styles.icon}  name='twitter' size={50}  color={'#3EC6FF'}/>
                  <Text>@mukhlis</Text>
                </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems:'center',
      flexDirection: 'column',
      
    },
    logo: {
        height: 200,
        width: 200,
        marginTop: 20,
        alignItems:'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 100
    },
    pageTitle: {
        color: '#003366',
        fontSize: 36,
        marginTop: 63,

    },
    Profile: {
        alignItems: 'center',
        marginTop: 20,
    },
    ProfileName: {
      color: '#003366',
      fontSize: 24,
      fontWeight: 'Bold',
    },
    ProfileJob: {
        color: '#3EC6FF',
        fontSize: 16,
        fontWeight: 'Bold',
        marginTop: 8,
    },
    textBox: { 
        width: 294,
        height: 48,
        borderColor: '#003366', 
        borderWidth: 1,
    },
    buttonLogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#3EC6FF",
        width: 140,
        height: 40,
        borderRadius: 16,
        marginTop: 32,
    },
    buttonRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#003366",
        width: 140,
        height: 40,
        borderRadius: 16,
    },
    input: {
        color: '#003366',
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 40,
    },
    box1: {
        backgroundColor: '#EFEFEF',
        marginTop: 16,
        width: 359,
        borderRadius: 16,
        alignItems: 'center',
        
    },
    box2: {
      borderTopWidth: 2,
      width: 343,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'center',
      color: '#003366',
    },
    box2_: {
      borderTopWidth: 2,
      width: 343,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'center',
      color: '#003366',
    },
    box3: {
      marginLeft:50,
      marginRight: 50,
      alignItems: 'center',
      color: '#003366',
      fontWeight: 'Bold',      
    },
    box3_: {
      width: 200,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'space-around',
      color: '#003366',
      fontWeight: 'Bold',
    },
    icon : {
      paddingRight: 10,
    },
});