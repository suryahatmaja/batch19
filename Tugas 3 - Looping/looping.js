//No. 1 Looping While
console.log("soal 1 - Looping While");
var up = 2;
console.log("LOOPING PERTAMA");
while (up <= 20)
{
    console.log(up + " - I love coding");
    up += 2;
}

var down = 20;
console.log("LOOPING KEDUA");
while (down > 0)
{
    console.log(down + " - I will become a mobile developer");
    down -= 2;
}

//No. 2 Looping menggunakan for
console.log("\nsoal 2 - Looping for");
for (var angka = 1; angka <= 20; angka++)
{
    if (angka%2 == 0)
    {
        console.log(angka + " - Berkualitas")
    }
    if (angka%2 != 0)
    {
        if (angka%3 == 0)
        {
            console.log(angka + " - I Love Coding")
        }
        else{
            console.log(angka + " - Santai")
        }
    }
}

//No. 3 Membuat Persegi Panjang
console.log("\nsoal 3 - Membuat Persegi Panjang");
var output = "";
for (var vertical = 0; vertical < 4; vertical++)
{
    for (var horizontal = 0; horizontal < 8; horizontal++)
    {
        output = output + "#";
    }
    console.log(output);
    output = "";
}

//No. 4 Membuat Tangga
console.log("\nsoal 4 - Membuat Tangga");
var ladder = "";
for (var vertical = 0; vertical < 7; vertical++)
{
    for (var horizontal = 0; horizontal <= vertical; horizontal++)
    {
        ladder = ladder + "#";
    }
    console.log(ladder);
    ladder = "";
}

//No. 5 Membuat Papan Catur
console.log("\nsoal 5 - Membuat Papan Catur")
var chess_board = "";
for (var vertical = 0; vertical < 8; vertical++)
{
    if (vertical%2 != 0)
    {
        for (var horizontal = 0; horizontal < 8; horizontal++)
        {
            if (horizontal%2 != 0)
            {
                chess_board = chess_board + " ";
            }
            else
            {
                chess_board = chess_board + "#";
            }
        }
    }
    else
    {
        for (var horizontal = 0; horizontal < 8; horizontal++)
        {
            if (horizontal%2 != 0)
            {
                chess_board = chess_board + "#";
            }
            else
            {
                chess_board = chess_board + " ";
            }
        }
    
    }
    console.log(chess_board);
    chess_board = "";
}