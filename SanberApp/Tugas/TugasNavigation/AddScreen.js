import * as React from 'react';
import { Button, View, Text } from 'react-native';

// Add
function AddScreen({ route, navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Add Screen</Text>
      <Button
        color="black"
        title="Home"
        onPress={() => navigation.navigate('Home')}
      />
      <Button
        color="red"
        title="Add something"
        onPress={() => navigation.push('Add')}
      />
    </View>
  );
}

export default AddScreen;