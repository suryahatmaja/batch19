//https://stackoverflow.com/questions/49826920/how-to-navigate-between-different-nested-stacks-in-react-navigation

import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './comp/LoginScreen';
import AboutScreen from './comp/AboutScreen';
import WorldScreen from './comp/WorldScreen';
import NationalScreen from './comp/NationalScreen';
import DashboardScreen from './comp/Dashboard';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';


const Tab = createBottomTabNavigator();

function HomeTabs() {
  return (
    <Tab.Navigator>  
       <Tab.Screen
        name="Home"
        component={DashboardScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="World"
        component={WorldScreen}
        options={{
          tabBarLabel: 'World',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="earth" color={color} size={size} />
          ),
        }}
      />
     <Tab.Screen
        name="National"
        component={NationalScreen}
        options={{
          tabBarLabel: 'National',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="near-me" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="About"
        component={AboutScreen}
        options={{
          tabBarLabel: 'About',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="information-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Logout"
        component={LoginScreen}
        options={{
          tabBarLabel: 'Exit',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="exit-run" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

function App() {
  
  return (
    <NavigationContainer>
      <Stack.Navigator 
        screenOptions={{ headerShown: false, }}>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ title: 'Login' }}
        />  
        <Stack.Screen name="Dashboard" component={HomeTabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default App;