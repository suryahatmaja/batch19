import * as React from 'react';
import { Button, View, Text } from 'react-native';

// Skill
function ProjectScreen({ route, navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Skill Screen</Text>
      <Button
        color="black"
        title="Home"
        onPress={() => navigation.navigate('Home')}
      />
    </View>
  );
}

export default ProjectScreen;