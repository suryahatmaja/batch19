//Soal No. 1 (Callback Baca Buku)
console.log("------------------------------------")
console.log("Soal No. 1 (Callback Baca Buku)");
console.log("------------------------------------")
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let availableTime = 10000;
let time;
let bookId = 0;
function letsRead()
{
    readBooks(availableTime, books[bookId], function(timeCheck){
        time = availableTime;
        availableTime = timeCheck;
        bookId++;
        if (bookId < books.length && availableTime < time)
        {
            letsRead();
        }
    });
}

letsRead();
