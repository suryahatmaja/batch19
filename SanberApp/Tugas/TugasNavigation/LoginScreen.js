
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AboutScreen from './AboutScreen';

function LoginScreen ({ navigation }) {
    return (
//      <View style={styles.container}>
      <KeyboardAvoidingView
        behavior = {Platform.OS == 'ios' ? 'padding' : 'height'}
        style = {styles.container}
      >
        <ScrollView>
           <View style = {styles.containerView}>
            <View style={styles.logo}>
                <Image source={require('./images/logo.png')} style={{ width: 375, height: 102 }} />  
            </View>

            <Text style={styles.pageTitle}>Login</Text>
            <View style = {styles.input}>
                <Text>Username / Email</Text>
                {/* <View>
                    <TextInput
                    style={styles.textBox}
                    onChangeText={(text) => this.setState({ value: text })}
                    value={this.state.value}
                    />
                    <Text>{this.state.value}</Text>
                </View>
                
                <Text style={{marginTop: 16}}>Password</Text>
                <View>
                    <TextInput
                    style={styles.textBox} secureTextEntry = {true}
                    />
                </View> */}
            </View>
            

            <TouchableOpacity style={styles.buttonLogin}
            onPress={() => navigation.navigate('About')}>
                <Text style={{color: '#FFFFFF', fontSize: 24,}}>Masuk</Text>            
            </TouchableOpacity>

            <Text style={{color: '#3EC6FF', fontSize: 24, marginTop: 16, marginBottom: 16,}}>atau</Text>

            <TouchableOpacity style={styles.buttonRegister}>
                <Text style={{color: '#FFFFFF', fontSize: 24,}}>Daftar ?</Text>            
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      //<View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,      
    },
    containerView: {
      alignItems:'center',
    },
    logo: {
        height: 110,
        width: 380,
        marginTop: 63,
    },
    pageTitle: {
        color: '#003366',
        fontSize: 24,
        marginTop: 70,
        textAlign:'center',

    },
    textBox: { 
        width: 294,
        height: 48,
        borderColor: '#003366', 
        borderWidth: 1,
    },
    buttonLogin: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#3EC6FF",
        width: 140,
        height: 40,
        borderRadius: 16,
        marginTop: 32,
    },
    buttonRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#003366",
        width: 140,
        height: 40,
        borderRadius: 16,
    },
    input: {
        color: '#003366',
        fontSize: 16,
        alignItems: 'flex-start',
        marginTop: 40,
    },
  });

  export default LoginScreen;